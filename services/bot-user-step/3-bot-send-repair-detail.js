exports.botSendRepairDetail = () => {
  let items = [];
  
  let repairDetail = ['เปิดไม่ติด', 'แอร์ไม่เย็น', 'หักหรือแตก', 'มีกลิ่นไหม้', 'จอดับ', 'น้ำไม่ไหล', 'ไฟดับ', 'อื่นๆ'];

  items = repairDetail.map((item) => {
    return {
        type: "action",
        action: {
            type: "postback",
            label: item,
            data: item
        }
    }
  });

  let msg = {
    type: "text",
    text: "กรุณาแจ้งอาการเสีย",
    quickReply: {
      items: items
    },
  };

  return msg;
};
